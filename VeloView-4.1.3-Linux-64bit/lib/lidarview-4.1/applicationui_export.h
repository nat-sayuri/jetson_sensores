
#ifndef APPLICATIONUI_EXPORT_H
#define APPLICATIONUI_EXPORT_H

#ifdef APPLICATIONUI_STATIC_DEFINE
#  define APPLICATIONUI_EXPORT
#  define APPLICATIONUI_NO_EXPORT
#else
#  ifndef APPLICATIONUI_EXPORT
#    ifdef ApplicationUi_EXPORTS
        /* We are building this library */
#      define APPLICATIONUI_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define APPLICATIONUI_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef APPLICATIONUI_NO_EXPORT
#    define APPLICATIONUI_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef APPLICATIONUI_DEPRECATED
#  define APPLICATIONUI_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef APPLICATIONUI_DEPRECATED_EXPORT
#  define APPLICATIONUI_DEPRECATED_EXPORT APPLICATIONUI_EXPORT APPLICATIONUI_DEPRECATED
#endif

#ifndef APPLICATIONUI_DEPRECATED_NO_EXPORT
#  define APPLICATIONUI_DEPRECATED_NO_EXPORT APPLICATIONUI_NO_EXPORT APPLICATIONUI_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef APPLICATIONUI_NO_DEPRECATED
#    define APPLICATIONUI_NO_DEPRECATED
#  endif
#endif

#endif /* APPLICATIONUI_EXPORT_H */
