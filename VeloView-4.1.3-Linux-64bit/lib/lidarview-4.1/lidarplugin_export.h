
#ifndef LIDARPLUGIN_EXPORT_H
#define LIDARPLUGIN_EXPORT_H

#ifdef LIDARPLUGIN_STATIC_DEFINE
#  define LIDARPLUGIN_EXPORT
#  define LIDARPLUGIN_NO_EXPORT
#else
#  ifndef LIDARPLUGIN_EXPORT
#    ifdef LidarPlugin_EXPORTS
        /* We are building this library */
#      define LIDARPLUGIN_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define LIDARPLUGIN_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef LIDARPLUGIN_NO_EXPORT
#    define LIDARPLUGIN_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef LIDARPLUGIN_DEPRECATED
#  define LIDARPLUGIN_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef LIDARPLUGIN_DEPRECATED_EXPORT
#  define LIDARPLUGIN_DEPRECATED_EXPORT LIDARPLUGIN_EXPORT LIDARPLUGIN_DEPRECATED
#endif

#ifndef LIDARPLUGIN_DEPRECATED_NO_EXPORT
#  define LIDARPLUGIN_DEPRECATED_NO_EXPORT LIDARPLUGIN_NO_EXPORT LIDARPLUGIN_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef LIDARPLUGIN_NO_DEPRECATED
#    define LIDARPLUGIN_NO_DEPRECATED
#  endif
#endif

#endif /* LIDARPLUGIN_EXPORT_H */
