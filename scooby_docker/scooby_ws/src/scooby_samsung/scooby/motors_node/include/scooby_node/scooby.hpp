/*******************************************************************************
* Copyright 2019 ROBOTIS CO., LTD.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*******************************************************************************/

/* Author: Olmerg */

#ifndef SCOOBY_NODE_SCOOBY_HPP_
#define SCOOBY_NODE_SCOOBY_HPP_

#include <array>
#include <chrono>
#include <list>
#include <memory>
#include <mutex>
#include <string>
#include <queue>

#include <geometry_msgs/msg/twist.hpp>
#include <nav_msgs/msg/odometry.hpp>
#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/battery_state.hpp>
#include <sensor_msgs/msg/imu.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <tf2_ros/transform_broadcaster.h>


#include "scooby_node/serial_wrapper.hpp"
#include "scooby_node/odometry.hpp"

#include "scooby_node/devices/devices.hpp"
#include "scooby_node/devices/motor_power.hpp"
#include "scooby_node/devices/reset.hpp"

#include "scooby_node/sensors/battery_state.hpp"
#include "scooby_node/sensors/imu.hpp"
#include "scooby_node/sensors/joint_state.hpp"
#include "scooby_node/sensors/sensor_state.hpp"
#include "scooby_node/sensors/sensors.hpp"

namespace lma
{
namespace scooby
{

class Scooby : public rclcpp::Node
{
 public:
  typedef struct
  {
    float separation;
    float radius;
  } Wheels;

  typedef struct
  {
    float profile_acceleration_constant;
    float profile_acceleration;
  } Motors;

  explicit Scooby(const std::string & ip_port);
  virtual ~Scooby(){};

  Wheels * get_wheels();
  Motors * get_motors();

 private:
  void init_serial_wrapper(const std::string & ip_port);

  void add_sensors();
  void add_devices();
  void add_motors();
  void add_wheels();

  void run();

  void publish_timer(const std::chrono::milliseconds timeout);

  void cmd_vel_callback();
  void parameter_event_callback();

  Wheels wheels_;
  Motors motors_;

  std::shared_ptr<SerialWrapper> serial_wrapper_;

  std::list<sensors::Sensors*> sensors_;
  std::map<std::string, devices::Devices*> devices_;

  std::unique_ptr<Odometry> odom_;

  rclcpp::Node::SharedPtr node_handle_;

  rclcpp::TimerBase::SharedPtr publish_timer_;

  rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr cmd_vel_sub_;

  rclcpp::AsyncParametersClient::SharedPtr priv_parameters_client_;
  rclcpp::Subscription<rcl_interfaces::msg::ParameterEvent>::SharedPtr parameter_event_sub_;
};
} // scooby
} // lma
#endif // SCOOBY_NODE_SCOOBY_HPP_
