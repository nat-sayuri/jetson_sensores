/*******************************************************************************
* Copyright 2019 ROBOTIS CO., LTD.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*******************************************************************************/

/* Author: Olmerg */

#ifndef SCOOBY_NODE_serial_wrapper_HPP_
#define SCOOBY_NODE_serial_wrapper_HPP_

#include <algorithm>
#include <array>
#include <chrono>
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <utility>

#include <rcutils/logging_macros.h>


#define LOG_INFO RCUTILS_LOG_INFO_NAMED
#define LOG_WARN RCUTILS_LOG_WARN_NAMED
#define LOG_ERROR RCUTILS_LOG_ERROR_NAMED
#define LOG_DEBUG RCUTILS_LOG_DEBUG_NAMED

/*#define READ_DATA_SIZE 56
#define WRITE_DATA_SIZE 16
#include "scooby_node/udp_socket.hpp"

*/
#include <serial/serial.h>

namespace lma
{
namespace scooby
{


class SerialWrapper
{
  private:
    //scooby::udp_socket _udp_socket;
    //std::vector<unsigned char> _buffer_tosend;
    //std::vector<unsigned char> _buffer_received;
    serial::Serial my_serial;
    
    bool simulation;
    double v_x;
    double w_z;
    
    double clock_1;
    float version;
 public:
    double clock;
    double battery;
    double w_r,theta_r;
    double w_l,theta_l;


    double w_r1, w_l1;


  explicit SerialWrapper(const std::string & p1,const int &baudrate);
  virtual ~SerialWrapper();

/**
 * This is a clamp function, which is available in c++17 but this software should be available with c++14
 * v 	- 	the value to clamp
 * lo,hi 	- 	the boundaries to clamp v to 
 * 
 * */
double  clamp(double & v,const double & lo,const double & hi);

  /**
   * Configure the speed desired of the vehicle
   * @param  v_x desired of the vehicle
   * @param w_z desired of the vehicel
   */

  void setSpeed(double v_x,double w_z);

  /**
   * Configure the speed desired of the vehicle
   * @param time wall time of ros2
   * @param E separation between the wheels
   * @param radius radius of the wheels
   * @return true if no errror in communication
   */
  bool refresh(const double &time,const double &E,const double &radius);


};
} // scooby
} // lma
#endif // scooby_NODE_DYNAMIXEL_SDK_WRAPPER_HPP_
