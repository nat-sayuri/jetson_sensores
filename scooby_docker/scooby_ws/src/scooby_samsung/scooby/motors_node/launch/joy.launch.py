# Copyright 2021 Olmer Garcia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Launch Gazebo with a world that has Scooby, as well as the follow node."""

import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription,TimerAction
from launch.conditions import IfCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node


def generate_launch_description():

    scooby_description_pkg=get_package_share_directory('scooby_description')
    scooby_node_pkg=get_package_share_directory('motors_node')
    use_robot_state_pub = LaunchConfiguration('use_robot_state_pub')
    urdf_file= LaunchConfiguration('urdf_file')

    robot_state_publisher = Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        name='robot_state_publisher',
        output='screen',
        parameters=[{'use_sim_time': False}],
        arguments=[urdf_file])

    odom_node = Node(
        package='motors_node',
        executable='scooby_ros',
        name='scooby_node',
        output='screen',
        parameters=[os.path.join(scooby_node_pkg, 'params', 'scooby.yaml')]
        )

    joy_linux = Node(package='joy_linux', executable='joy_linux_node', parameters=[{'dev': '/dev/input/js0'}])
        
    # Caminho para o arquivo de configuração YAML
    parameters_file = os.path.join(
        scooby_node_pkg,
        'param', 'joy_teleop.yaml'
    )

    ld = LaunchDescription([
        DeclareLaunchArgument('urdf_file',default_value=os.path.join(scooby_description_pkg, 'urdf', 'scooby.urdf'),
                              description='urddeclare_urdf_cmd =f file complete path'),  

        # Criação do parâmetro teleop_config cujo valor padrão é o parameters_file que contém o caminho para o YAML
        DeclareLaunchArgument('teleop_config', default_value=parameters_file),                 
        
        odom_node,
        joy_linux,
        robot_state_publisher
    ])

    # Definição do nó joy_teleop passando como parâmetro teleop_config
    ld.add_action(Node(package='joy_teleop', executable='joy_teleop', parameters=[LaunchConfiguration('teleop_config')]))
            
    return ld
