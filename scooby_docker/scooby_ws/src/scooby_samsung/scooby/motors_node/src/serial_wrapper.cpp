/*******************************************************************************
* Copyright 2021 LMA-UNICAMP.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*******************************************************************************/
// por favor adicionar dialout group to the user: sudo usermod -a -G dialout name_user y reiniciar

/* Author: Olmerg */

#include "scooby_node/serial_wrapper.hpp"
#include <string>
#include <iostream>
#include <unistd.h>
#include <thread>         // std::this_thread::sleep_for
#include <chrono> 
#include <cmath> 
using namespace lma;
using namespace scooby;

SerialWrapper::SerialWrapper(const std::string & p1,const int &baudrate)
{
  
  my_serial.setPort(p1);
  my_serial.setBaudrate(baudrate);
  //void 	setTimeout (uint32_t inter_byte_timeout, uint32_t read_timeout_constant, uint32_t read_timeout_multiplier, uint32_t write_timeout_constant, uint32_t write_timeout_multiplier)
  my_serial.setTimeout(serial::Timeout::max(),10, 0, 10, 0);
  

  try {
  simulation=false;
  my_serial.open();
  sleep(2);
  std::cout << " serial ports ready "<< my_serial.getPort()<<std::endl;
 
   //LOG_DEBUG("SerialWrapper", "Success to open Port");
  } catch (std::exception &e) {
     std::cout  << "Simulation mode -> Exception: " << e.what() << std::endl;
    simulation=true;
   //  LOG_DEBUG("SerialWrapper", "Simulation mode");
  }

this->v_x=0;
this->w_z=0;
      
this->clock_1 = -1;

}

SerialWrapper::~SerialWrapper()
{
  ;
}
void SerialWrapper::setSpeed(double  v_x,double w_z){
  this->v_x=v_x;
  this->w_z=w_z;

}
double  SerialWrapper::clamp(double & v,const double & lo,const double & hi){
  return (v < lo) ? lo : (hi < v) ? hi : v;
}
bool SerialWrapper::refresh(const double &time,const double &E,const double &radius)
{
  //TODO: tipo de mensagen, enviar clock do computador para verificar errors no sppedgoat......
  //double E=0.4,r=0.1;
  double fator = 1;//2000.0;
  double w_ld=1/radius*(v_x-E*w_z)*fator;
  double w_rd=1/radius*(v_x+E*w_z)*fator;

  //std::cout << "w_ld: " << w_ld << " , w_rd: " <<  w_rd << " , v_x: " << v_x << " , w_z: " << w_z << std::endl;


  double delta=0.1;
  if (!simulation){
    
    w_ld=this->clamp(w_ld,-10.0,10.0);
    w_rd=this->clamp(w_rd,-10.0,10.0);


    //std::cout << "1) w_ld: " << w_ld << " , w_rd: " << w_rd << std::endl;

    if((w_rd-this->w_r1)>delta){
      w_rd=this->w_r1+delta;
    }else if((this->w_r1-w_rd)>delta){
      w_rd=this->w_r1-delta;
    }
    if((w_ld-this->w_l1)>delta){
      w_ld=this->w_l1+delta;
    }else if((this->w_l1-w_ld)>delta){
      w_ld=this->w_l1-delta;
    }
   
    this->w_r1 = w_rd;
    this->w_l1 = w_ld;

   //my_serial.write("z=1 vl=1000 vr=1000 m=1 \n");
   
   //std::cout << "1.1) w_ld: " << w_ld << " , w_rd: " << w_rd << std::endl;

   my_serial.write("z=1 vl="+std::to_string((int)(w_ld*1000.0))+" vr="+std::to_string((int)(w_rd*1000.0)) +" m=1 \n");

   //std::cout << "vl: " << (int)(w_ld*1000) << " , " << "vr: " << (int)(w_rd*1000) << std::endl;

   // my_serial2.write("z=1 y="+std::to_string((int)(w_rd*1000))+" m=1\n");
    //usleep(1000);
    std::this_thread::sleep_for (std::chrono::milliseconds(1));
    int v=(int)w_ld,t=0;
    int v2=(int)w_rd;
    float b=0;
    int checksum=0;  
    std::string result = my_serial.readline(64,"\n\n");
    if(result.length()>0){ 
      sscanf(result.c_str(),"%d,%d,%d,%f,%d",&v,&v2,&t,&b,&checksum);
    }
  
    //std::cout << "V: " << v <<" , V2: " << v2 <<" , T: "<< t << " , W_RD: " << w_rd << " , W_LD: "<< w_ld << std::endl;
    //std::cout << "result.length: " << result.length() << " , checksum: " << checksum << std::endl;

    if(t>0 && result.length() == checksum){
      this->w_r=((double)v)/25000.0;
      this->w_l=((double)v2)/25000.0;
      this->clock=((double)(t))/1000.0; 
      this->battery=b;

      if (this->clock_1 > 0)
      {
        double dt=this->clock-this->clock_1;  
        this->theta_r+=this->w_r*dt;
        this->theta_l+=this->w_l*dt;
      }
      this->clock_1=clock;
    
      //std::cout << "w_l: " << w_l << " , w_r: " << w_r << " , theta_r: " << this->theta_r << " , theta_l: " << this->theta_l <<  std::endl;
    
    }
    
  }else{ //simulation mode
    double dt=time-this->clock_1;  
    w_l=w_ld;
    w_r=w_rd;
    this->theta_r+=this->w_r*dt;
    this->theta_l+=this->w_l*dt;
    std::cout << "simulation mode time_step:"<<dt<<std::endl;
    this->clock=time;
    this->clock_1=clock; 
  }
    

  return true;
}

