
//  Copyright (c) 2003-2020 Xsens Technologies B.V. or subsidiaries worldwide.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
//
//  1.	Redistributions of source code must retain the above copyright notice,
//  	this list of conditions, and the following disclaimer.
//
//  2.	Redistributions in binary form must reproduce the above copyright notice,
//  	this list of conditions, and the following disclaimer in the documentation
//  	and/or other materials provided with the distribution.
//
//  3.	Neither the names of the copyright holders nor the names of their contributors
//  	may be used to endorse or promote products derived from this software without
//  	specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
//  THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
//  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
//  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY OR
//  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.THE LAWS OF THE NETHERLANDS
//  SHALL BE EXCLUSIVELY APPLICABLE AND ANY DISPUTES SHALL BE FINALLY SETTLED UNDER THE RULES
//  OF ARBITRATION OF THE INTERNATIONAL CHAMBER OF COMMERCE IN THE HAGUE BY ONE OR MORE
//  ARBITRATORS APPOINTED IN ACCORDANCE WITH SAID RULES.
//

#include "xdainterface.h"

#include <xscontroller/xsscanner.h>
#include <xscontroller/xscontrol_def.h>
#include <xscontroller/xsdevice_def.h>

#include <xstypes/xsoutputconfigurationarray.h>

#include "messagepublishers/packetcallback.h"
#include "messagepublishers/accelerationpublisher.h"
#include "messagepublishers/angularvelocitypublisher.h"
#include "messagepublishers/freeaccelerationpublisher.h"
#include "messagepublishers/gnsspublisher.h"
#include "messagepublishers/imupublisher.h"
#include "messagepublishers/magneticfieldpublisher.h"
#include "messagepublishers/orientationincrementspublisher.h"
#include "messagepublishers/orientationpublisher.h"
#include "messagepublishers/pressurepublisher.h"
#include "messagepublishers/temperaturepublisher.h"
#include "messagepublishers/timereferencepublisher.h"
#include "messagepublishers/transformpublisher.h"
#include "messagepublishers/twistpublisher.h"
#include "messagepublishers/velocityincrementpublisher.h"
#include "messagepublishers/positionllapublisher.h"
#include "messagepublishers/velocitypublisher.h"



XdaInterface::XdaInterface(std::shared_ptr<rclcpp::Node> &node)
	: m_device(nullptr), node_(node)
{
	
	RCLCPP_INFO(node_->get_logger(), "Creating XsControl object...");	
	m_control = XsControl::construct();
	assert(m_control != 0);
	
	RCLCPP_INFO(node_->get_logger(), "Creating Parameters ...");	
	
	node_->declare_parameter<int>("publisher_queue_size", 5);
	node_->declare_parameter<bool>("pub_imu", false);
	node_->declare_parameter<bool>("pub_quaternion", false);
	node_->declare_parameter<bool>("pub_acceleration", false);
	node_->declare_parameter<bool>("pub_angular_velocity", false);	
	node_->declare_parameter<bool>("pub_sampletime", false);
	node_->declare_parameter<bool>("pub_temperature", false);	
	node_->declare_parameter<bool>("pub_pressure", false);
	node_->declare_parameter<bool>("pub_gnss", false);
	node_->declare_parameter<bool>("pub_twist", false);
	node_->declare_parameter<bool>("pub_free_acceleration", false);	
	node_->declare_parameter<bool>("pub_transform", false);
	node_->declare_parameter<bool>("pub_positionLLA", false);
	node_->declare_parameter<bool>("pub_velocity", false);
	node_->declare_parameter("baudrate");
	node_->declare_parameter("device_id");
	node_->declare_parameter("port");
	node_->declare_parameter("log_file");
	
}

XdaInterface::~XdaInterface()
{	
	RCLCPP_INFO(node_->get_logger(), "Cleaning up ...");
	close();
	m_control->destruct();
}

void XdaInterface::spinFor(std::chrono::milliseconds timeout)
{
	RosXsDataPacket rosPacket = m_xdaCallback.next(timeout);

	if (!rosPacket.second.empty())
	{
		for (auto &cb : m_callbacks)
		{
			cb->operator()(rosPacket.second, rosPacket.first);
		}
	}
}

void XdaInterface::registerPublishers()
{
	bool should_publish;
	
	bool has = node_->has_parameter("port");
	
	std::string portName;
	node_->get_parameter<std::string>("port",portName);
	
	RCLCPP_INFO(node_->get_logger(), "has port: %d. get port: ...%s... empty: %d",has,portName.c_str(),portName.empty());
	
	
	if(node_->get_parameter<bool>("pub_imu",should_publish) && should_publish)
	{		
		registerCallback(new ImuPublisher(node_));
	}
	if(node_->get_parameter<bool>("pub_quaternion",should_publish) && should_publish)
	{
		registerCallback(new OrientationPublisher(node_));
	}
	if(node_->get_parameter<bool>("pub_acceleration",should_publish) && should_publish)
	{
		registerCallback(new AccelerationPublisher(node_));
	}
	if(node_->get_parameter<bool>("pub_angular_velocity",should_publish) && should_publish)
	{
		registerCallback(new AngularVelocityPublisher(node_));
	}
	if(node_->get_parameter<bool>("pub_mag",should_publish) && should_publish)
	{
		registerCallback(new MagneticFieldPublisher(node_));
	}
	if(node_->get_parameter<bool>("pub_dq",should_publish) && should_publish)
	{
		registerCallback(new OrientationIncrementsPublisher(node_));
	}
	if(node_->get_parameter<bool>("pub_dv",should_publish) && should_publish)
	{
		registerCallback(new VelocityIncrementPublisher(node_));
	}
	if(node_->get_parameter<bool>("pub_sampletime",should_publish) && should_publish)
	{
		registerCallback(new TimeReferencePublisher(node_));
	}
	if(node_->get_parameter<bool>("pub_temperature",should_publish) && should_publish)
	{
		registerCallback(new TemperaturePublisher(node_));
	}
	if(node_->get_parameter<bool>("pub_pressure",should_publish) && should_publish)
	{
		registerCallback(new PressurePublisher(node_));
	}
	if(node_->get_parameter<bool>("pub_gnss",should_publish) && should_publish)
	{
		registerCallback(new GnssPublisher(node_));
	}
	if(node_->get_parameter<bool>("pub_twist",should_publish) && should_publish)
	{
		registerCallback(new TwistPublisher(node_));
	}
	if(node_->get_parameter<bool>("pub_free_acceleration",should_publish) && should_publish)
	{
		registerCallback(new FreeAccelerationPublisher(node_));
	}
	if(node_->get_parameter<bool>("pub_transform",should_publish) && should_publish)
	{
		registerCallback(new TransformPublisher(node_));
	}
	if(node_->get_parameter<bool>("pub_positionLLA",should_publish) && should_publish)
	{
		registerCallback(new PositionLLAPublisher(node_));
	}
	if(node_->get_parameter<bool>("pub_velocity",should_publish) && should_publish)
	{
		registerCallback(new VelocityPublisher(node_));
	}
	
	
}

bool XdaInterface::connectDevice()
{
	// Read baudrate parameter if set
	XsBaudRate baudrate = XBR_Invalid;
	
	int baudrateParam = 0;
	
	node_->get_parameter_or<int>("baudrate",baudrateParam, 0);
	
	if(baudrateParam != 0)
	{	
		RCLCPP_INFO(node_->get_logger(), "Found baudrate parameter: %d", baudrateParam);
		baudrate = XsBaud::numericToRate(baudrateParam);
	}
	
	// Read device ID parameter
	bool checkDeviceID = false;
	std::string deviceId;
	node_->get_parameter<std::string>("device_id",deviceId);
		
	if(!deviceId.empty())	
	{	
		checkDeviceID = true;
		RCLCPP_INFO(node_->get_logger(), "Found device ID parameter: %s.",deviceId.c_str());
	}
	// Read port parameter if set
	XsPortInfo mtPort;
	
	
	std::string portName;
	node_->get_parameter<std::string>("port",portName);
	
	if(!portName.empty())	
	{	
		RCLCPP_INFO(node_->get_logger(), "Found port name parameter: %s", portName.c_str());
		mtPort = XsPortInfo(portName, baudrate);
		RCLCPP_INFO(node_->get_logger(), "Scanning port %s ...", portName.c_str());
		
		if (!XsScanner::scanPort(mtPort, baudrate))
			return handleError("No MTi device found. Verify port and baudrate.");
		if (checkDeviceID && mtPort.deviceId().toString().c_str() != deviceId)
			return handleError("No MTi device found with matching device ID.");

	}
	else
	{
		RCLCPP_INFO(node_->get_logger(), "Scanning for devices...");
		XsPortInfoArray portInfoArray = XsScanner::scanPorts(baudrate);

		for (auto const &portInfo : portInfoArray)
		{
			if (portInfo.deviceId().isMti() || portInfo.deviceId().isMtig())
			{
				if (checkDeviceID)
				{
					if (portInfo.deviceId().toString().c_str() == deviceId)
					{
						mtPort = portInfo;
						break;
					}
				}
				else
				{
					mtPort = portInfo;
					break;
				}
			}
		}
	}

	if (mtPort.empty())
		return handleError("No MTi device found.");

	RCLCPP_INFO(node_->get_logger(), "Found a device with ID: %s @ port: %s, baudrate: %d", mtPort.deviceId().toString().toStdString().c_str(), mtPort.portName().toStdString().c_str(), XsBaud::rateToNumeric(mtPort.baudrate()));

	RCLCPP_INFO(node_->get_logger(), "Opening port %s ...", mtPort.portName().toStdString().c_str());
	
	if (!m_control->openPort(mtPort))
		return handleError("Could not open port");

	m_device = m_control->device(mtPort.deviceId());
	assert(m_device != 0);

	RCLCPP_INFO(node_->get_logger(), "Device: %s, with ID: %s opened.", m_device->productCode().toStdString().c_str(), m_device->deviceId().toString().c_str());
	
	m_device->addCallbackHandler(&m_xdaCallback);

	return true;
}

bool XdaInterface::prepare()
{
	assert(m_device != 0);

	if (!m_device->gotoConfig())
		return handleError("Could not go to config");

	// read EMTS and device config stored in .mtb file header.
	if (!m_device->readEmtsAndDeviceConfiguration())
		return handleError("Could not read device configuration");

	XsOutputConfigurationArray configArray;
	configArray.push_back(XsOutputConfiguration(XDI_PacketCounter, 0));
	configArray.push_back(XsOutputConfiguration(XDI_SampleTimeFine, 0));

	//std::cout << "1 - isImu" << std::endl;
	configArray.push_back(XsOutputConfiguration(XDI_Acceleration, 0));
	configArray.push_back(XsOutputConfiguration(XDI_RateOfTurn, 0));
	configArray.push_back(XsOutputConfiguration(XDI_MagneticField, 0));
	configArray.push_back(XsOutputConfiguration(XDI_Quaternion, 0));

	if (!m_device->setOutputConfiguration(configArray))
	{
		RCLCPP_ERROR(node_->get_logger(), "Could not configure MTi device. Aborting.");
		return handleError("Could not configure MTi device. Aborting.");
	}


	RCLCPP_INFO(node_->get_logger(), "Measuring ...");
	
	if (!m_device->gotoMeasurement())
		return handleError("Could not put device into measurement mode");

	std::string logFile;
	node_->get_parameter<std::string>("log_file",logFile);
	if(!logFile.empty())
	{
		if (m_device->createLogFile(logFile) != XRV_OK)
			return handleError("Failed to create a log file! (" + logFile + ")");
		else
			RCLCPP_INFO(node_->get_logger(), "Created a log file: %s", logFile.c_str());

		RCLCPP_INFO(node_->get_logger(), "Recording to %s ...", logFile.c_str());
		if (!m_device->startRecording())
			return handleError("Could not start recording");
	}

	return true;
}

void XdaInterface::close()
{
	if (m_device != nullptr)
	{
		m_device->stopRecording();
		m_device->closeLogFile();
		m_device->removeCallbackHandler(&m_xdaCallback);
	}
	m_control->closePort(m_port);
}

void XdaInterface::registerCallback(PacketCallback *cb)
{
	m_callbacks.push_back(cb);
}

bool XdaInterface::handleError(std::string error)
{
	RCLCPP_ERROR(node_->get_logger(),"%s", error.c_str());
	close();
	return false;
}
