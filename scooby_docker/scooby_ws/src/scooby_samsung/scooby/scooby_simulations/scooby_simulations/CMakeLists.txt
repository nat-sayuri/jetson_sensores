cmake_minimum_required(VERSION 3.5)
project(scooby_simulations)
find_package(ament_cmake REQUIRED)
ament_package()
